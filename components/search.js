import React from 'react';
import {StyleSheet,View,TextInput,Button,FlatList,Text,ActivityIndicator} from 'react-native';
import films from '../helpers/filmsData'
import FilmItem from "./FilmItem"
import {getFilmFRomApiSearch} from '../API/TMBApi'



class Search extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            films: [],
            isLoading: false
        }
        this.SearchedText =  ""
    }

    _loadFilms(){
        this.setState({isLoading:true})
        if (this.SearchedText.length > 0){
            getFilmFRomApiSearch(this.SearchedText).then(data =>this.setState({
                films:data.results,
                isLoading:false
            }))
        }
    }

    _displayLoading(){
        if (this.state.isLoading){
            return(
                <View style={styles.loading_conatainer}>
                    <ActivityIndicator size="large"/>
                </View>
            )
        }
    }
    _searchInputText(text){
        this.SearchedText = text
    }
    render() {
        console.log(this.state.isLoading)
        return(
            <View style={styles.maincontainer}>
                <TextInput onSubmitEditin={()=>this._loadFilms()} onChangeText={(text)=>this._searchInputText(text)} style={styles.textinput} placeholder ="Titre du film" />
                <Button style={{height:50}} title="Rechercher" onPress={() =>this._loadFilms()}/>
                <FlatList
                    data={this.state.films}
                    keyExtractor = { item=> item.id}
                    renderItem={({ item }) => <FilmItem film={item}/>}
                />
                {this._displayLoading()}
            </View>
        )
    }
}

const styles= StyleSheet.create({
    maincontainer: {
        marginTop: 20,
        flex : 1
    },
    textinput: {
        marginRight:5,
        marginLeft:5,
        height:50,
        borderColor:'#000000',
        borderWidth : 1,
        paddingLeft:5
    },
    loading_container:{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 100,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    }
});
export default Search