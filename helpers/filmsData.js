export default data = [
    {
        id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
        title: 'First Item',
        description: 'bonjour 1',
        vote: '255',
        dates : '25/01/2015'
    },
    {
        id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
        title: 'Second Item',
        description: 'bonjour 2',
        vote: '250',
        dates : '25/02/2015'
    },
    {
        id: '58694a0f-3da1-471f-bd96-145571e29d72',
        title: 'Third Item',
        description: 'bonjour 3',
        vote: '25',
        dates : '28/01/2015'
    },
];