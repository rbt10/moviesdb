const API_TOKEN ='74230c47fcf753f8b08a3980f0df93c4'
export function getFilmFRomApiSearch(text) {
    const url = 'https://api.themoviedb.org/3/search/movie?api_key=' + API_TOKEN + '&language=fr&query=' + text
    return fetch(url)
        .then((response) => response.json())
        .catch((error)=>console.log(error))
}
export function getImageFromApi(name) {
    return 'https://image.tmdb.org/t/p/w300' + name
}